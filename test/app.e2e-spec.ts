import { stringify } from 'querystring';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { PairRate, RUB, USD } from '../src/app.interface';
import { getRates } from '../src/lib/get-rates';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );
    await app.init();
    await app
      .getHttpAdapter()
      .getInstance()
      .ready();
  });

  it('/ (GET)', () => {
    const query = stringify({
      price: 1,
      currency: 'RUB',
      countryCode: 'RU',
    });

    return request(app.getHttpServer())
      .get(`/?${query}`)
      .expect(200)
      .expect({
        price: 1,
        currency: 'RUB',
      });
  });

  it('/ (GET) RUB -> USD', async () => {
    const rates: PairRate = await getRates();
    const price = 1;

    const query = stringify({
      price,
      currency: 'USD',
      countryCode: 'RU',
    });

    return request(app.getHttpServer())
      .get(`/?${query}`)
      .expect(200)
      .expect({
        price: price * rates[`${USD}${RUB}`],
        currency: 'RUB',
      });
  });
});
