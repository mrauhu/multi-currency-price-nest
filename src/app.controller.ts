import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { LocalePrice } from './app.interface';
import { LocalePriceDto } from './dto/locale-price.dto';
import { LocalePricePipe } from './pipes/locale-price.pipe';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getLocalePrice(
    @Query(new LocalePricePipe()) query: LocalePriceDto,
  ): Promise<LocalePrice> {
    return this.appService.getLocalePrice(query);
  }
}
