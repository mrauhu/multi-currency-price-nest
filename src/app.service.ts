import { countries, Country } from 'countries-list';
import { Injectable } from '@nestjs/common';
import { Currency, EUR, LocalePrice, RUB, USD } from './app.interface';
import { LocalePriceDto } from './dto/locale-price.dto';
import { getRates } from './lib/get-rates';

// Creating map for getting a currency by the country code
const indexedCounties: { [index: string]: Country } = countries;
const knownCountryCodeCurrency = new Map<string, Currency>();
Object.keys(countries).forEach(code => {
  const { currency } = indexedCounties[code];
  if (currency === EUR || currency === RUB) {
    knownCountryCodeCurrency.set(code, currency);
  }
});

@Injectable()
export class AppService {
  async getLocalePrice({
    price,
    currency,
    countryCode,
  }: LocalePriceDto): Promise<LocalePrice> {
    const code = countryCode.toUpperCase();
    const toCurrency: Currency = knownCountryCodeCurrency.has(code)
      ? <typeof EUR | typeof RUB>knownCountryCodeCurrency.get(code)
      : USD;
    // Same currency
    if (currency === toCurrency) {
      return {
        currency,
        price,
      };
    }

    const rates = await getRates();
    const rate = rates[`${currency}${toCurrency}`];

    return {
      currency: toCurrency,
      price: price * rate,
    };
  }
}
