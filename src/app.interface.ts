export const EUR = 'EUR';
export const RUB = 'RUB';
export const USD = 'USD';

export type Currency = typeof EUR | typeof RUB | typeof USD;

export interface LocalePrice {
  price: number;
  currency: Currency;
}

export interface ParseResult {
  currency: Currency;
  rate: number;
}

export interface PairRate {
  [index: string]: number;
}
