import { ApiProperty } from '@nestjs/swagger';
import { IsISO31661Alpha2, IsNumber, IsString, Length } from 'class-validator';
import { Transform } from 'class-transformer';
import { Currency, EUR, RUB, USD } from '../app.interface';
import { countries } from 'countries-list';

// Reorder
delete countries.RU;
const countryCodes = ['RU', ...Object.keys(countries)];

export class LocalePriceDto {
  @ApiProperty()
  @Transform(price => parseFloat(price))
  @IsNumber()
  price: number;

  @ApiProperty({
    enum: [RUB, EUR, USD],
  })
  @IsString()
  @Length(3, 3)
  currency: Currency;

  @ApiProperty({
    enum: countryCodes,
  })
  @IsString()
  @IsISO31661Alpha2()
  countryCode: string;
}
