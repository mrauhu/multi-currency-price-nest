import * as NodeCache from 'node-cache';
import { fetch } from './fetch';
import { parse } from './parse';
import { createPairs } from './create-pairs';
import { PairRate } from '../app.interface';

const KEY = 'rates';

const cache = new NodeCache({
  stdTTL: parseInt(<string>process.env.CACHE_TTL_SEC, 10) || 300,
});

export async function getRates(): Promise<PairRate> {
  // Try get from cache
  let rates = <PairRate>cache.get(KEY);
  if (rates) {
    return rates;
  }

  const xml = await fetch();
  const parsed = await parse(xml);
  rates = createPairs(parsed);
  // Store in cache
  cache.set(KEY, rates);
  return rates;
}
