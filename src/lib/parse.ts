import { transform } from 'camaro';
import { ParseResult } from '../app.interface';

const TEMPLATE = [
  'gesmes:Envelope/Cube/Cube/Cube',
  {
    currency: '@currency',
    rate: '@rate',
  },
];

export async function parse(xml: string): Promise<ParseResult[]> {
  return transform(xml, TEMPLATE);
}
