import { EUR, PairRate, ParseResult, RUB, USD } from '../app.interface';

export function createPairs(parsed: ParseResult[]): PairRate {
  const pairs: PairRate = {};
  const currencies = [USD, RUB];
  let found = 0;
  // EUR pairs
  for (let i = 0; i < parsed.length; i++) {
    const { currency, rate } = parsed[i];
    const key = currencies.indexOf(currency);
    if (key >= 0) {
      pairs[`${EUR}${currency}`] = rate;
      pairs[`${currency}${EUR}`] = 1 / rate;
      ++found;
    }
    if (found === currencies.length) {
      break;
    }
  }
  // RUB and USD pairs
  currencies.forEach((currency, key) => {
    const from = currency;
    const to = key === 0 ? currencies[key + 1] : currencies[key - 1];
    pairs[`${from}${to}`] = pairs[`${from}${EUR}`] * pairs[`${EUR}${to}`];
  });

  return pairs;
}
