import axios from 'axios';

const XML_URL = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';

export async function fetch() {
  const { data: xml } = await axios.get(XML_URL);
  return xml;
}
