import { PipeTransform, Injectable } from '@nestjs/common';
import { LocalePriceDto } from '../dto/locale-price.dto';

@Injectable()
export class LocalePricePipe implements PipeTransform {
  transform({ price, currency, countryCode }: LocalePriceDto) {
    // Fix: strange behavior in e2e test
    return {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      price: parseFloat(price),
      currency,
      countryCode,
    };
  }
}
