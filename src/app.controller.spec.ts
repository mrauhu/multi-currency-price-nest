import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { getRates } from './lib/get-rates';
import { PairRate, RUB, USD } from './app.interface';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return same currency and price', async () => {
      expect(
        await appController.getLocalePrice({
          countryCode: 'RU',
          currency: 'RUB',
          price: 1,
        }),
      ).toStrictEqual({
        currency: 'RUB',
        price: 1,
      });
    });

    it('should convert price', async () => {
      const rates: PairRate = await getRates();
      const price = 1;

      expect(
        await appController.getLocalePrice({
          countryCode: 'RU',
          currency: 'USD',
          price: 1,
        }),
      ).toStrictEqual({
        currency: 'RUB',
        price: price * rates[`${USD}${RUB}`],
      });
    });
  });
});
